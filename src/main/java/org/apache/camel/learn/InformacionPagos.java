package org.apache.camel.learn;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord (separator = ",")
public class InformacionPagos {
    @DataField(pos=1)
    private String id;
    @DataField(pos=2)
    private String limite;
    @DataField(pos=3)
    private String sexo;
    @DataField(pos=4)
    private String educacion;
    @DataField(pos=5)
    private String casado;
    @DataField(pos=6)
    private String edad;
    @DataField(pos=7)
    private String pago0;
    @DataField(pos=8)
    private String pago2;
    @DataField(pos=9)
    private String pago3;
    @DataField(pos=10)
    private String pago4;
    @DataField(pos=11)
    private String pago5;
    @DataField(pos=12)
    private String pago6;
    @DataField(pos=13)
    private String factura1;
    @DataField(pos=14)
    private String factura2;
    @DataField(pos=15)
    private String factura3;
    @DataField(pos=16)
    private String factura4;
    @DataField(pos=17)
    private String factura5;
    @DataField(pos=18)
    private String factura6;
    @DataField(pos=19)
    private String payAmt1;
    @DataField(pos=20)
    private String payAmt2;
    @DataField(pos=21)
    private String payAmt3;
    @DataField(pos=22)
    private String payAmt4;
    @DataField(pos=23)
    private String payAmt5;
    @DataField(pos=24)
    private String payAmt6;
    @DataField(pos=25)
    private String defaultPaymentNextMonth;

    // Getters y Setters para id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    // Getters y Setters para limitBal
    public String getLimite() {
        return limite;
    }

    public void setLimite(String limitBal) {
        this.limite = limitBal;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEducacion() {
        return educacion;
    }

    public void setEducacion(String educacion) {
        this.educacion = educacion;
    }

    public String getCasado() {
        return casado;
    }

    public void setCasado(String casado) {
        this.casado = casado;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getPago0() {
        return pago0;
    }

    public void setPago0(String pago0) {
        this.pago0 = pago0;
    }

    public String getPago2() {
        return pago2;
    }

    public void setPago2(String pago2) {
        this.pago2 = pago2;
    }

    public String getPago3() {
        return pago3;
    }

    public void setPago3(String pago3) {
        this.pago3 = pago3;
    }

    public String getPago4() {
        return pago4;
    }

    public void setPago4(String pago4) {
        this.pago4 = pago4;
    }

    public String getPago5() {
        return pago5;
    }

    public void setPago5(String pago5) {
        this.pago5 = pago5;
    }

    public String getPago6() {
        return pago6;
    }

    public void setPago6(String pago6) {
        this.pago6 = pago6;
    }

    public String getFactura1() {
        return factura1;
    }

    public void setFactura1(String factura1) {
        this.factura1 = factura1;
    }

    public String getFactura2() {
        return factura2;
    }

    public void setFactura2(String factura2) {
        this.factura2 = factura2;
    }

    public String getFactura3() {
        return factura3;
    }

    public void setFactura3(String factura3) {
        this.factura3 = factura3;
    }

    public String getFactura4() {
        return factura4;
    }

    public void setFactura4(String factura4) {
        this.factura4 = factura4;
    }

    public String getFactura5() {
        return factura5;
    }

    public void setFactura5(String factura5) {
        this.factura5 = factura5;
    }

    public String getFactura6() {
        return factura6;
    }

    public void setFactura6(String factura6) {
        this.factura6 = factura6;
    }

    public String getPayAmt1() {
        return payAmt1;
    }

    public void setPayAmt1(String payAmt1) {
        this.payAmt1 = payAmt1;
    }

    public String getPayAmt2() {
        return payAmt2;
    }

    public void setPayAmt2(String payAmt2) {
        this.payAmt2 = payAmt2;
    }

    public String getPayAmt3() {
        return payAmt3;
    }

    public void setPayAmt3(String payAmt3) {
        this.payAmt3 = payAmt3;
    }

    public String getPayAmt4() {
        return payAmt4;
    }

    public void setPayAmt4(String payAmt4) {
        this.payAmt4 = payAmt4;
    }

    public String getPayAmt5() {
        return payAmt5;
    }

    public void setPayAmt5(String payAmt5) {
        this.payAmt5 = payAmt5;
    }

    public String getPayAmt6() {
        return payAmt6;
    }

    public void setPayAmt6(String payAmt6) {
        this.payAmt6 = payAmt6;
    }

    public String getDefaultPaymentNextMonth() {
        return defaultPaymentNextMonth;
    }

    public void setDefaultPaymentNextMonth(String defaultPaymentNextMonth) {
        this.defaultPaymentNextMonth = defaultPaymentNextMonth;
    }

    
}
