package org.apache.camel.learn;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends RouteBuilder {

    DataFormat bindy = new BindyCsvDataFormat(org.apache.camel.learn.InformacionPagos.class);

    /**
     * Let's configure the Camel routing rules using Java code...
     */
    public void configure(){

        //Para llamar al buzon sftp
        from("sftp:localhost:2222/upload?noop=true&username=foo&password=pass")
        .unmarshal(bindy)
        .split(body())
        .streaming()
        .process(new Procesador())
        .to("direct:procesarInformacion");

        //para procesar la informacion
        from("direct:procesarInformacion")
        .filter(simple("${body.id} != 'ID'")) 
        .process( new ProcesadorInformacion())
        .to("jdbc:myDataSource"); 
    }

}
