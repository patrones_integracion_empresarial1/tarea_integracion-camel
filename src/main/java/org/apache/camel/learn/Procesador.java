package org.apache.camel.learn;
import org.apache.logging.log4j.LogManager; 
import org.apache.logging.log4j.Logger;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class Procesador implements Processor {
    private String mensajeValidacion;
        private int datosCorrectos = 0;
        private int datosErroneos = 0;
    

    public static final Logger log = LogManager.getLogger(Procesador.class); 
    
    @Override
    public void process(Exchange exchange) throws Exception {
        InformacionPagos infoPagos =  exchange.getIn().getBody(InformacionPagos.class);
        String nombreArchivo = (String) exchange.getIn().getHeader("CamelFileNameOnly");

         if ( infoPagos.getId().equals("ID")){
            log.info("");
            log.info("**************************************************************************************************************");
            log.info("****************************** Inicio Lectura del archivo" + nombreArchivo +" ***********************************");
            log.info("**************************************************************************************************************");
        }

         
        mensajeValidacion = "";
        if (!infoPagos.getId().equals("ID")){
            //Validacion: El valor de la factura a pagar no puede ser menor o igual a cero.
            if (Double.valueOf(infoPagos.getFactura1()) <= 0.0 ||Double.valueOf(infoPagos.getFactura2()) <= 0.0 ||
                Double.valueOf(infoPagos.getFactura3()) <= 0.0 ||Double.valueOf(infoPagos.getFactura4()) <= 0.0 ||
                Double.valueOf(infoPagos.getFactura5()) <= 0.0 ||Double.valueOf(infoPagos.getFactura6()) <= 0.0 ){
                mensajeValidacion = "Registro con ID: "+ infoPagos.getId()+ " - No cumple validacion: El valor de la factura a pagar no puede ser menor o igual a cero.";
                log.info(mensajeValidacion);
            }

            //El valor pagado no puede ser menor o igual a cero.
            if (Double.valueOf(infoPagos.getPayAmt1()) <= 0.0 ||Double.valueOf(infoPagos.getPayAmt2()) <= 0.0 ||
                Double.valueOf(infoPagos.getPayAmt3()) <= 0.0 ||Double.valueOf(infoPagos.getPayAmt4()) <= 0.0 ||
                Double.valueOf(infoPagos.getPayAmt5()) <= 0.0 ||Double.valueOf(infoPagos.getPayAmt6()) <= 0.0 ) {
                mensajeValidacion = "Registro con ID: "+ infoPagos.getId()+ " - No cumple validacion: El valor pagado no puede ser menor o igual a cero.";
                log.info(mensajeValidacion);
            }
            
    
            if (mensajeValidacion.compareTo("") != 0){
                datosErroneos++;
                infoPagos.setId("ID");
            }
            else {
                datosCorrectos++;
            }

        boolean ultimaFila = exchange.getProperty(Exchange.SPLIT_COMPLETE, boolean.class);

        if (ultimaFila) {
            log.info("**************************************************************************************************************");
            log.info("Datos incorrectos:              " + datosErroneos);
            log.info("Datos insertados correctamente: " + datosCorrectos);
            log.info("Datos totales:                  " + (datosCorrectos + datosErroneos));
            log.info("**************************************************************************************************************");
            log.info("******************************* Fin Lectura del archivo " + nombreArchivo +" ************************************");
            log.info("**************************************************************************************************************");
            log.info("");
            
            datosErroneos = 0;
            datosCorrectos = 0;
        }
    }
}
}