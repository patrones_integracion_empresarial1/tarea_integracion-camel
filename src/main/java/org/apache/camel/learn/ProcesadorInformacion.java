package org.apache.camel.learn;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProcesadorInformacion implements Processor{

     //Declarar el Logger
    public static final Logger log = LogManager.getLogger(ProcesadorInformacion.class);

    // Declarar el contador
    private int datosCorrectos = 0;
    private int datosErroneos = 0;

    @Override
    public void process(Exchange exchange) throws Exception {
        
        InformacionPagos infoPagos = exchange.getIn().getBody(InformacionPagos.class);

        // Insert tabla
        String insertStatement =   "INSERT INTO public.informacion_pagos (ID, LIMITE, SEXO, EDUCACION, CASADO, EDAD, PAGO_0, PAGO_2, PAGO_3, PAGO_4, PAGO_5, PAGO_6, FACTURA_1, FACTURA_2, FACTURA_3, FACTURA_4, FACTURA_5, FACTURA_6, PAY_AMT_1, PAY_AMT_2, PAY_AMT_3, PAY_AMT_4, PAY_AMT_5, PAY_AMT_6, DEFAULT_PAYMENT_NEXT_MONTH) "+
                "VALUES (" +
                infoPagos.getId() + ", " +
                infoPagos.getLimite() + ", " +
                infoPagos.getSexo() + ", " +
                infoPagos.getEducacion() + ", " +
                infoPagos.getCasado() + ", " +
                infoPagos.getEdad() + ", " +
                infoPagos.getPago0() + ", " +
                infoPagos.getPago2() + ", " +
                infoPagos.getPago3() + ", " +
                infoPagos.getPago4() + ", " +
                infoPagos.getPago5() + ", " +
                infoPagos.getPago6() + ", " +
                infoPagos.getFactura1() + ", " +
                infoPagos.getFactura2() + ", " +
                infoPagos.getFactura3() + ", " +
                infoPagos.getFactura4() + ", " +
                infoPagos.getFactura5() + ", " +
                infoPagos.getFactura6() + ", " +
                infoPagos.getPayAmt1() + ", " +
                infoPagos.getPayAmt2() + ", " +
                infoPagos.getPayAmt3() + ", " +
                infoPagos.getPayAmt4() + ", " +
                infoPagos.getPayAmt5() + ", " +
                infoPagos.getPayAmt6() + ", " +
                infoPagos.getDefaultPaymentNextMonth() + 
                ");  ";
                exchange.getIn().setBody(insertStatement); 
                
    } 
}
