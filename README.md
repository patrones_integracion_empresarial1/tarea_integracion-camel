# Patrones de Integración Empresarial

## Realizado por
Eduardo Aguilar Riera.

## Codigo versionado en:
https://gitlab.com/patrones_integracion_empresarial1/tarea_integracion-camel.git

## Archivos Importantes

### Logs
En la carpeta logs/logSalidaPractica.log

### Pruebas realizadas
Integración fuera de linea - Eduardo Aguilar R.pdf



## Ejecucion del proyecto CMD 1
### Corremos una base postgres con la sentencia
docker run --name postgres_edu -e POSTGRES_USER=camel -e POSTGRES_PASSWORD=camel -p 5432:5432 -d postgres:15

### Levantamos el contenedor para acceder a la base de datos
docker exec -it postgres_edu psql -U camel -d postgres

### Creamos la tabla
CREATE TABLE public.informacion_pagos (
    CODIGO SERIAL PRIMARY KEY,
    ID INT,
    LIMITE NUMERIC(10, 2),
    SEXO INT,
    EDUCACION INT,
    CASADO INT,
    EDAD INT,
    PAGO_0 INT,
    PAGO_2 INT,
    PAGO_3 INT,
    PAGO_4 INT,
    PAGO_5 INT,
    PAGO_6 INT,
    FACTURA_1 NUMERIC(10, 2),
    FACTURA_2 NUMERIC(10, 2),
    FACTURA_3 NUMERIC(10, 2),
    FACTURA_4 NUMERIC(10, 2),
    FACTURA_5 NUMERIC(10, 2),
    FACTURA_6 NUMERIC(10, 2),
    PAY_AMT_1 NUMERIC(10, 2),
    PAY_AMT_2 NUMERIC(10, 2),
    PAY_AMT_3 NUMERIC(10, 2),
    PAY_AMT_4 NUMERIC(10, 2),
    PAY_AMT_5 NUMERIC(10, 2),
    PAY_AMT_6 NUMERIC(10, 2),
    DEFAULT_PAYMENT_NEXT_MONTH INT
);

## Ejecucion del proyecto CMD 2
### Levantamos un servidor sftp
docker run -p 2222:22 -d atmoz/sftp foo:pass:::upload

### Nos vamos a la ruta donde esta la practica tarea_integracion-camel>cd src\data 
cd src\data

### Nos conectamos al servidor sftp (en caso de pedir clave es pass)
sftp -P 2222 foo@localhost

### Copiamos los archivos en el buzon sftp (antes de abrir el sftp estamos en la ruta src\data)
put cardsclients3.csv

## Ejecucion del proyecto CMD 3

### Instalar el proyecto
mvn clean install

### Correr el proyecto
mvn camel:run

### Verificamos los registros en la base de datos (CMD 1)
select * from public.informacion_pagos;
select count(1) from public.informacion_pagos;